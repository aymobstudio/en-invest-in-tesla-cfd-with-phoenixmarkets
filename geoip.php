<?php

$ip = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];

$ipArr = explode(",",  $ip);
$user_ip = $ipArr[0];
$ip_address = $user_ip;

// get user country info based on ip (autoselect country in form select)
$ch = curl_init();
// curl_setopt($ch, CURLOPT_URL, 'https://geoip.maxmind.com/geoip/v2.1/country/'.$user_ip.'?pretty');
curl_setopt($ch, CURLOPT_URL, 'https://geoip.maxmind.com/geoip/v2.1/city/'.$user_ip.'?pretty');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($ch, CURLOPT_USERPWD, '137891' . ':' . 'id7T2ur2Gf8kik2O');
$geoRes = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);
$geoDecoded = json_decode($geoRes, true);

$geo = [
    // 'country' => $geoDecoded['country']['iso_code'],
    'country' => $geoDecoded['country']['names']['en'],
    'citizenship' => $geoDecoded['country']['iso_code'],
    'ip' =>  $geoDecoded['traits']['ip_address'],
    'city' =>  $geoDecoded['city']['names']['en'],
    'postal' =>  $geoDecoded['postal']['code']
];
