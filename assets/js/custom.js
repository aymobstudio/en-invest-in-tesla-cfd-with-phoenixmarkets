let $ = jQuery;

function stickyHeader() {
  const header = document.querySelector("#sticky-btn");
  const scrollUp = "sticky";
  const scrollDown = "sticky-up";
  let lastScroll = 0;

  window.addEventListener("scroll", () => {
    const currentScroll = window.pageYOffset;
    if (currentScroll <= 250) {
      header.classList.remove(scrollUp);
      header.classList.remove(scrollDown);
      return;
    }

    if (currentScroll > lastScroll && !header.classList.contains(scrollDown)) {
      // down
      header.classList.remove(scrollUp);
      header.classList.add(scrollDown);
    } else if (
      currentScroll < lastScroll &&
      header.classList.contains(scrollDown)
    ) {
      // up
      header.classList.remove(scrollDown);
      header.classList.add(scrollUp);
    }
    lastScroll = currentScroll;
  });
}

stickyHeader();

let passwordInput = document.getElementById('password'),
  toggle = document.getElementById('btnToggle');

document.addEventListener("DOMContentLoaded", () => {

  function togglePassword() {
    if (passwordInput.type === 'password') {
      passwordInput.type = 'text';
      toggle.innerHTML = `
        <svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M11 3.99995C14.79 3.99995 18.17 6.12995 19.82 9.49995C19.23 10.72 18.4 11.77 17.41 12.62L18.82 14.03C20.21 12.8 21.31 11.26 22 9.49995C20.27 5.10995 16 1.99995 11 1.99995C9.73 1.99995 8.51 2.19995 7.36 2.56995L9.01 4.21995C9.66 4.08995 10.32 3.99995 11 3.99995ZM9.93 5.13995L12 7.20995C12.57 7.45995 13.03 7.91995 13.28 8.48995L15.35 10.56C15.43 10.22 15.49 9.85995 15.49 9.48995C15.5 7.00995 13.48 4.99995 11 4.99995C10.63 4.99995 10.28 5.04995 9.93 5.13995ZM1.01 1.86995L3.69 4.54995C2.06 5.82995 0.77 7.52995 0 9.49995C1.73 13.89 6 17 11 17C12.52 17 13.98 16.71 15.32 16.18L18.74 19.6L20.15 18.19L2.42 0.449951L1.01 1.86995ZM8.51 9.36995L11.12 11.98C11.08 11.99 11.04 12 11 12C9.62 12 8.5 10.88 8.5 9.49995C8.5 9.44995 8.51 9.41995 8.51 9.36995ZM5.11 5.96995L6.86 7.71995C6.63 8.26995 6.5 8.86995 6.5 9.49995C6.5 11.98 8.52 14 11 14C11.63 14 12.23 13.87 12.77 13.64L13.75 14.62C12.87 14.86 11.95 15 11 15C7.21 15 3.83 12.87 2.18 9.49995C2.88 8.06995 3.9 6.88995 5.11 5.96995Z" fill="white"/>
        </svg>
      `;
    } else {
      passwordInput.type = 'password';
      toggle.innerHTML = `
        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M12 6.5C15.79 6.5 19.17 8.63 20.82 12C19.17 15.37 15.8 17.5 12 17.5C8.2 17.5 4.83 15.37 3.18 12C4.83 8.63 8.21 6.5 12 6.5ZM12 4.5C7 4.5 2.73 7.61 1 12C2.73 16.39 7 19.5 12 19.5C17 19.5 21.27 16.39 23 12C21.27 7.61 17 4.5 12 4.5ZM12 9.5C13.38 9.5 14.5 10.62 14.5 12C14.5 13.38 13.38 14.5 12 14.5C10.62 14.5 9.5 13.38 9.5 12C9.5 10.62 10.62 9.5 12 9.5ZM12 7.5C9.52 7.5 7.5 9.52 7.5 12C7.5 14.48 9.52 16.5 12 16.5C14.48 16.5 16.5 14.48 16.5 12C16.5 9.52 14.48 7.5 12 7.5Z" fill="#323232"/>
        </svg>
      `;
    }
  }

  toggle.addEventListener('click', togglePassword, false);

});

// Master form STARTS

const regCheckBox = document.getElementById("custom-checkbox-input");
const regActionBtn = document.getElementById("create-account-button");

let validation = {
  name: false,
  email: false,
  pass: false,
  phone: false,
  checkbox: true,
}

$("#hero-form").submit(function (e) {

  e.preventDefault();

  let form = $(this);
  let url = $(form).attr('action');

  $.ajax({
    type: "POST",
    url: url,
    dataType: "json",
    data: form.serialize(),
    beforeSend: function (xhr) {
      $('#create-account-button').attr('disabled', true).html("Processing...");
    },
    success: function (res) {
      $('#create-account-button').attr('disabled', false).html(`Create account`);
      form.find('input').val("");
      form.trigger('reset');
      if (res.status == 'false') {
        $('#error-box').fadeIn().html(res.message);
        setTimeout(function () {
          $('#error-box').fadeOut().html();
        }, 5000);
      } else {
        location.href = `thankyou.php?clickid=${res.clickid}&td=${res.td}`;
        console.log('Success');
      }
    }
  });
});
const phoneInputField = document.querySelector("#phone");
const phoneInput = window.intlTelInput(phoneInputField, {
  initialCountry: "auto",
  geoIpLookup: function (success) {
    // Get your api-key at https://ipdata.co/
    fetch("https://api.ipdata.co/?api-key=fb6483095ab6d6ef96d67904a2d8fe9e05c462b3d7063c54f5024f89")
      .then(function (response) {
        if (!response.ok) return success("");
        return response.json();
      })
      .then(function (ipdata) {
        success(ipdata.country_code);
      });
  },
  utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js?1613236686837", // just for formatting/placeholders etc
});

regActionBtn.addEventListener('click', function () {
  let code = phoneInput.getNumber();
  phoneInputField.value = code;
});

// phone number validation
$('#phone').first().keyup(function () {
  let phone = this.value;
  if (phone.length < 5) {
    document.getElementById("phone-message").innerHTML = "This field is required.";
    validation.phone = false;
    checkValidations();
  } else {
    document.getElementById("phone-message").innerHTML = " ";
    validation.phone = true;
    checkValidations();
    return true;
  }
});

const checkName = function () {
  let fname = document.getElementById("fname").value;
  let regExp = fname.split(" ").length <= 2 ? new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})") : new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{1,}\\s[a-zA-Z]{2,})");
  if ((fname.length > 32) || (fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname) || (fname.split(" ").length > 3)) {
    document.getElementById("fname-message").innerHTML = "The full name field is not valid.";
    validation.name = false;
    checkValidations();
    return false;
  }
  document.getElementById("fname-message").innerHTML = " ";
  validation.name = true;
  checkValidations();
  return true;
}

const checkEmail = function () {
  let email = document.getElementById("email").value;
  const emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if ((email.length < 2) || (!emailReg.test(email))) {
    document.getElementById("email-message").innerHTML = "Please enter a valid email.";
    validation.email = false;
    checkValidations();
  } else {
    document.getElementById("email-message").innerHTML = " ";
    validation.email = true;
    checkValidations();
    return true;
  }
}

const checkPassword = function () {
  let pw = document.getElementById("password").value;
  if ((pw.length < 8) || (pw.search(/[a-z]/) < 0) || (pw.search(/[A-Z]/) < 0) || (pw.search(/[0-9]/) < 0) || (pw.length > 15)) {
    document.getElementById("password-message").innerHTML = "The password length must be 8-15 characters long.";
    validation.pass = false;
    checkValidations();
    return false;
  }
  document.getElementById("password-message").innerHTML = " ";
  validation.pass = true;
  checkValidations();
  return true;
}

const checkValidations = function () {
  Object.values(validation).some(val => val === false) ?
    regActionBtn.setAttribute("disabled", "disabled") :
    regActionBtn.removeAttribute("disabled");
}

regCheckBox.addEventListener('click', function () {
  validation.checkbox = !validation.checkbox;
  checkValidations();
});

(function () {
  if (!(validation.name && validation.email && validation.pass && validation.phone && validation.checkbox)) regActionBtn.setAttribute("disabled", "disabled");
})();

// Master form ENDS

const modalTrigger = document.querySelectorAll("[data-modal]"),
  modal = document.querySelector(".reg-window"),
  modalCloseBtn = document.querySelector("[data-close]");

modalTrigger.forEach((btn) => {
  btn.addEventListener("click", openModal);
});

function closeModal() {
  modal.classList.add("hide");
  modal.classList.remove("show");
  document.body.style.overflow = "";
}

function openModal() {
  modal.classList.add("show");
  modal.classList.remove("hide");
  document.body.style.overflow = "hidden";
}

modalCloseBtn.addEventListener("click", closeModal);

modal.addEventListener("click", (e) => {
  if (e.target === modal) {
    closeModal();
  }
});

document.addEventListener("keydown", (e) => {
  if (e.code === "Escape" && modal.classList.contains("show")) {
    closeModal();
  }
});

$(".site-footer-menu, .site-footer ul a").on("click", function () {
  // $(this).find("ul").toggle();
  openModal();
});


// Calculator

function calc() {
  document.getElementById("value_deposit").innerHTML = document.getElementById("deposit").value,
    document.getElementById("value_time").innerHTML = document.getElementById("time").value;
  let time = parseInt(document.getElementById("time").value),
    deposit = parseInt(document.getElementById("deposit").value),
    pricecalc = deposit + deposit * time * .09;
  document.getElementById("pricecalc").innerHTML = pricecalc.toFixed(2)
}

calc();


// Reg form STARTS

const regFormCheckBox = document.getElementById("reg-checkbox-input");
const regFormActionBtn = document.getElementById("reg-form-btn");

let regFormValidation = {
  name: false,
  email: false,
  pass: false,
  phone: false,
  checkbox: true,
}

$("#interest-form").submit(function (e) {

  e.preventDefault();

  let form = $(this);
  let url = $(form).attr('action');

  $.ajax({
    type: "POST",
    url: url,
    dataType: "json",
    data: form.serialize(),
    beforeSend: function (xhr) {
      $('#reg-form-btn').attr('disabled', true).html("Processing...");
    },
    success: function (res) {
      $('#reg-form-btn').attr('disabled', false).html(`START TRADING`);
      form.find('input').val("");
      form.trigger('reset');
      if (res.status == 'false') {
        $('#reg-error-box').fadeIn().html(res.message);
        setTimeout(function () {
          $('#reg-error-box').fadeOut().html();
        }, 5000);
      } else {
        location.href = `thankyou.php?clickid=${res.clickid}&td=${res.td}`;
        console.log('Success');
      }
    }
  });
});
const regFormPhoneInputField = document.querySelector("#reg-phone");
const regFormPhoneInput = window.intlTelInput(regFormPhoneInputField, {
  initialCountry: "auto",
  geoIpLookup: function (success) {
    // Get your api-key at https://ipdata.co/
    fetch("https://api.ipdata.co/?api-key=fb6483095ab6d6ef96d67904a2d8fe9e05c462b3d7063c54f5024f89")
      .then(function (response) {
        if (!response.ok) return success("");
        return response.json();
      })
      .then(function (ipdata) {
        success(ipdata.country_code);
      });
  },
  utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js?1613236686837", // just for formatting/placeholders etc
});

regFormActionBtn.addEventListener('click', function () {
  let code = regFormPhoneInput.getNumber();
  regFormPhoneInputField.value = code;
});

// phone number validation
$('#reg-phone').first().keyup(function () {
  let phone = this.value;
  if (phone.length < 5) {
    document.getElementById("reg-phone-message").innerHTML = "This field is required.";
    regFormValidation.phone = false;
    regFormCheckValidations();
  } else {
    document.getElementById("reg-phone-message").innerHTML = " ";
    regFormValidation.phone = true;
    regFormCheckValidations();
    return true;
  }
});

const regFormCheckName = function () {
  let fname = document.getElementById("reg-fname").value;
  let regExp = fname.split(" ").length <= 2 ? new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})") : new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{1,}\\s[a-zA-Z]{2,})");
  if ((fname.length > 32) || (fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname) || (fname.split(" ").length > 3)) {
    document.getElementById("reg-fname-message").innerHTML = "The full name field is not valid.";
    regFormValidation.name = false;
    regFormCheckValidations();
    return false;
  }
  document.getElementById("reg-fname-message").innerHTML = " ";
  regFormValidation.name = true;
  regFormCheckValidations();
  return true;
}

const regFormCheckEmail = function () {
  let email = document.getElementById("reg-email").value;
  const emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if ((email.length < 2) || (!emailReg.test(email))) {
    document.getElementById("reg-email-message").innerHTML = "Please enter a valid email.";
    regFormValidation.email = false;
    regFormCheckValidations();
  } else {
    document.getElementById("reg-email-message").innerHTML = " ";
    regFormValidation.email = true;
    regFormCheckValidations();
    return true;
  }
}

const regFormCheckPassword = function () {
  let pw = document.getElementById("reg-password").value;
  if ((pw.length < 8) || (pw.search(/[a-z]/) < 0) || (pw.search(/[A-Z]/) < 0) || (pw.search(/[0-9]/) < 0) || (pw.length > 15)) {
    document.getElementById("reg-password-message").innerHTML = "The password length must be 8-15 characters long.";
    regFormValidation.pass = false;
    regFormCheckValidations();
    return false;
  }
  document.getElementById("reg-password-message").innerHTML = " ";
  regFormValidation.pass = true;
  regFormCheckValidations();
  return true;
}

const regFormCheckValidations = function () {
  Object.values(regFormValidation).some(val => val === false) ?
    regFormActionBtn.setAttribute("disabled", "disabled") :
    regFormActionBtn.removeAttribute("disabled");
    console.log(regFormValidation);
}

regFormCheckBox.addEventListener('click', function () {
  regFormValidation.checkbox = !regFormValidation.checkbox;
  regFormCheckValidations();
});

(function () {
  if (!(regFormValidation.name && regFormValidation.email && regFormValidation.pass && regFormValidation.phone && regFormValidation.checkbox)) regFormActionBtn.setAttribute("disabled", "disabled");
})();